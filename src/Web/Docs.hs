-- | API documentation description

{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Web.Docs where

-- Servant
import Servant              (QueryParam, Capture)
import Servant.Docs         (
                              DocQueryParam(..)
                            , ToSample(..)
                            , ToParam(..)
                            , ToCapture(..)
                            , ParamKind(Normal)
                            , DocCapture(..)
                            )

-- DB
import DB.Query.QueryTypes  (Infinitive, Conjugation)

instance ToCapture (Capture "verb" Infinitive) where
    toCapture _ =
        DocCapture "verb"
                   "A Spanish verb in the infinitive form"

instance ToParam (QueryParam "tense" String) where
    toParam _ =
        DocQueryParam "tense"
                  [
                      "presente"
                    , "futuro"
                    , "imperfecto"
                    , "preterito"
                    , "condicional"
                    , "presente perfecto"
                    , "futuro perfecto"
                    , "pluscuamperfecto"
                    , "preterito anterior"
                    , "condicional perfecto"
                   ]
                  "Tense of the Spanish verb"
                  Normal

instance ToParam (QueryParam "mood" String) where
    toParam _ =
        DocQueryParam "mood"
                  [
                      "indicativo"
                    , "subjunctivo"
                    , "imperativo afirmativo"
                    , "imperativo negativo"
                  ]
                  "Mood of the Spanish verb"
                  Normal

instance ToSample [Conjugation] [Conjugation] where
    toSample _ = Just []  -- @TODO
