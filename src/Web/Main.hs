-- | API definition and end-point handlers

{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeFamilies  #-}
{-# LANGUAGE TypeOperators #-}

module Web.Main (ESAPI, verbDetail) where

-- DB
import DB.Main                    (conjugate, conjugateAll)
import DB.Query.QueryTypes        (Mood, Tense, Conjugation, Infinitive)

-- Servant
import Servant                    (Get, JSON, ServantErr, QueryParam, (:>), Capture)

-- Control
import Control.Monad.Trans        (liftIO)
import Control.Monad.Trans.Either (EitherT)

type ESAPI =
    "verbs" :> Capture    "verb"  Infinitive :>
               QueryParam "tense" Tense      :>
               QueryParam "mood"  Mood       :> Get '[JSON] [Conjugation]

verbDetail :: Infinitive  ->  -- Verb infinitive
              Maybe Tense ->  -- Verb tense
              Maybe Mood  ->  -- Verb mood
              EitherT ServantErr IO [Conjugation]  -- Verb Conjugations
verbDetail i (Just t) Nothing  = liftIO $ conjugate i t m where m = "indicativo"
verbDetail i Nothing  (Just m) = liftIO $ conjugate i t m where t = "presente"
verbDetail i (Just t) (Just m) = liftIO $ conjugate i t m
verbDetail i _        _        = liftIO $ conjugateAll i
