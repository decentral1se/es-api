-- | High level PostgreSQL interacting functions

module DB.Main (
    conjugate
  , conjugateAll
) where

-- Data
import Data.Char             (toUpper)

-- DB
import DB.Query.QueryDB      (queryBuilder, queryExec)
import DB.Query.QueryTypes   (Conjugation, Infinitive, Tense, Mood)
import DB.Query.QueryStrings (conjugationQuery, allConjugationQuery)

-- | Conjugate the verb 'i' in the tense 't' and mood 'm'.
-- > conjugate "ser" "presente" "indicativo"
conjugate :: Infinitive -> Tense -> Mood -> IO [Conjugation]
conjugate i t m = queryExec $
    queryBuilder conjugationQuery [  i     :: Infinitive
                                   , cap t :: Tense
                                   , cap m :: Mood
                                  ]

-- | Conjugate the verb 'i' in all tenses and moods
-- > conjugateAll "ser"
conjugateAll :: Infinitive -> IO [Conjugation]
conjugateAll i = queryExec $ queryBuilder allConjugationQuery [i :: Infinitive]

-- | Capitalise a string
-- > cap "presente"
cap :: String -> String
cap (c:cs) = toUpper c : cs
