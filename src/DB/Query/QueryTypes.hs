-- | Spanish language domain related types

{-# LANGUAGE DeriveGeneric #-}

module DB.Query.QueryTypes (
    Infinitive
  , Tense
  , PastParticiple
  , Gerund
  , FormValue
  , Mood
  , EngMeaning
  , Conjugation
) where

-- Aeson
import Data.Aeson                         (ToJSON)

-- Generics
import GHC.Generics                       (Generic)

-- PostgreSQL
import Database.PostgreSQL.Simple.FromRow (FromRow(..), field)

type Infinitive     = String
type Tense          = String
type PastParticiple = String
type Gerund         = String
type FormValue      = String
type Mood           = String
type EngMeaning     = String

-- | A Verb Conjugation
data Conjugation = Conjugation {
    infinitive           :: Infinitive,
    firstPersonSingular  :: FormValue,
    secondPersonSingular :: FormValue,
    thirdPersonSingular  :: FormValue,
    firstPersonPlural    :: FormValue,
    secondPersonPlural   :: FormValue,
    thirdPersonPlural    :: FormValue,
    mood                 :: Mood,
    tense                :: Tense,
    englishMeaning       :: EngMeaning
} deriving (Show, Generic)

instance ToJSON Conjugation

instance FromRow Conjugation where
    fromRow = Conjugation <$> field <*> field
                          <*> field <*> field
                          <*> field <*> field
                          <*> field <*> field
                          <*> field <*> field
