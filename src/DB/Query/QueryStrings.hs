-- | PostgreSQL query strings

{-# LANGUAGE QuasiQuotes #-}

module DB.Query.QueryStrings (
    conjugationQuery
  , allConjugationQuery
) where

-- PostgreSQL
import Database.PostgreSQL.Simple      (Query)

-- Text
import Text.InterpolatedString.Perl6   (q)

-- | A single conjugation of infinitive 'i' in tense 't' and mood 'm'
conjugationQuery :: Query
conjugationQuery = [q|
    SELECT infinitive, form_1s, form_2s, form_3s,
           form_1p, form_2p, form_3p, mood, tense, verb_english
    FROM   verbs
    WHERE  infinitive=? AND tense=? AND mood=?
|]

-- | All possible conjugation combinations of infinitive 'i'
allConjugationQuery :: Query
allConjugationQuery = [q|
    SELECT infinitive, form_1s, form_2s, form_3s,
           form_1p, form_2p, form_3p, mood, tense, verb_english
    FROM   verbs
    WHERE  infinitive=?
|]
