-- | Database connectivity and query builder/execution

module DB.Query.QueryDB (
    queryBuilder
  , queryExec
  , DB
) where

-- System
import System.Environment           (getEnv)

-- Control
import Control.Monad.Reader         (ask, lift, runReaderT, ReaderT)

-- PostgreSQL
import Database.PostgreSQL.Simple   (
                                       Query
                                     , FromRow
                                     , ToRow
                                     , Connection
                                     , query
                                     , connect
                                     , defaultConnectInfo
                                     , connectHost
                                     , connectUser
                                     , connectPassword
                                     , connectDatabase
                                    )

-- | A database connection
connection :: IO Connection
connection = do
    host <- getEnv "ES_HOST"
    user <- getEnv "ES_USER"
    pw   <- getEnv "ES_PWORD"
    db   <- getEnv "ES_DB"
    connect defaultConnectInfo {  connectHost     = host
                                , connectUser     = user
                                , connectPassword = pw
                                , connectDatabase = db
                               }

-- | newtype wrapper for ReaderT Monad Transformer. This new type
-- | exposes a database connection as the ask-able state
newtype DB a = MkDB { unDB :: ReaderT Connection IO a }

-- | Run Database ReaderT
runDB :: ReaderT Connection IO a -> IO a
runDB dbReader = runReaderT dbReader =<< connection

-- | Construct a DB type
queryBuilder :: (ToRow paramTypes, FromRow a) => Query -> paramTypes -> DB [a]
queryBuilder queryString paramTypes = MkDB $ do
    conn <- ask
    lift  $ query conn queryString paramTypes

-- | Execute a database query
queryExec :: DB a -> IO a
queryExec built = runDB $ unDB $ built
