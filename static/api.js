
function getverbs(verb, tense, mood, onSuccess, onError)
{
  $.ajax(
    { url: '/verbs/' + encodeURIComponent(verb) + '' + '?tense=' + encodeURIComponent(tense) + '&mood=' + encodeURIComponent(mood)
    , success: onSuccess
    , error: onError
    , type: 'GET'
    });
}
